Locally run - Docker-compose:

```
$ docker-compose build
$ docker-compose up
```

Swagger available at:
http://localhost:8002/swagger/


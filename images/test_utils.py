from io import BytesIO

from PIL import Image


def create_test_image(width: int, height: int):
    file = BytesIO()
    image = Image.new("RGBA", size=(width, height), color=(155, 0, 0))
    image.save(file, "png")
    file.name = "test.png"
    file.seek(0)
    return file

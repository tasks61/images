from PIL import Image, ImageOps
from rest_framework.exceptions import ValidationError

from images.models import Picture

from rest_framework.serializers import (  # isort:skip
    IntegerField,
    ModelSerializer,
    SerializerMethodField,
)


class PictureCreateSerializer(ModelSerializer):
    url = SerializerMethodField()
    width = IntegerField(required=False, write_only=True)
    height = IntegerField(required=False, write_only=True)

    class Meta:
        model = Picture
        fields = ("id", "title", "image", "url", "width", "height")
        extra_kwargs = {
            "image": {"write_only": True},
        }

    def get_url(self, picture):
        request = self.context.get("request")
        image_url = picture.image.url
        return request.build_absolute_uri(image_url)

    def validate(self, data):
        width = data.get("width", None)
        height = data.get("height", None)
        if width and height and (width < 1 or height < 1):
            raise ValidationError("Width and height must be positive values.")

        return data

    def create(self, validated_data):
        width = validated_data.pop("width", None)
        height = validated_data.pop("height", None)

        picture = super().create(validated_data)

        if width and height:
            image = ImageOps.exif_transpose(Image.open(picture.image))
            image = image.resize((width, height), Image.ANTIALIAS)
            image.save(picture.image.path, quality=100)

        return picture


class PictureSerializer(ModelSerializer):
    url = SerializerMethodField()
    width = SerializerMethodField()
    height = SerializerMethodField()

    class Meta:
        model = Picture
        fields = ("id", "title", "url", "width", "height")

    def get_url(self, picture):
        request = self.context.get("request")
        image_url = picture.image.url
        return request.build_absolute_uri(image_url)

    def get_width(self, picture):
        return picture.image.width

    def get_height(self, picture):
        return picture.image.height

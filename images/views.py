from rest_framework.viewsets import ModelViewSet

from images.filters import PictureFilter
from images.models import Picture
from images.serializers import PictureCreateSerializer, PictureSerializer


class PictureView(ModelViewSet):
    queryset = Picture.objects.all()
    serializer_class = PictureSerializer
    filterset_class = PictureFilter
    http_method_names = ["get", "post"]

    def get_serializer_class(self):
        """Returns serializer depends on request method."""
        if self.request.method == "POST":
            return PictureCreateSerializer
        else:
            return super().get_serializer_class()

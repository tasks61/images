from django.conf import settings
from django.db import models


class Picture(models.Model):
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to=settings.IMAGE_FOLDER_NAME)

    class Meta:
        ordering = ("id",)

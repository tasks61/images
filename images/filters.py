from django_filters import CharFilter, FilterSet, OrderingFilter

from images.models import Picture


class PictureFilter(FilterSet):

    title = CharFilter(field_name="title", lookup_expr="icontains")

    ordering = OrderingFilter(fields=(("id", "id"), ("title", "title")))

    class Meta:
        model = Picture
        fields = ["title"]

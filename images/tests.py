import os
import random

from django.conf import settings
from django.test import TestCase, override_settings
from django.urls import reverse
from rest_framework.test import APIClient

from images.factories import PictureFactory
from images.models import Picture
from images.test_utils import create_test_image

FILES_PATH = "/tmp/files"
IMAGE_FOLDER_PATH = f"{FILES_PATH}/{settings.IMAGE_FOLDER_NAME}"


@override_settings(
    DEFAULT_FILE_STORAGE="django.core.files.storage.FileSystemStorage",
    MEDIA_ROOT=FILES_PATH,
)
class PictureViewTest(TestCase):
    def setUp(self):
        super().setUp()
        self.client = APIClient()

        self.width = random.randint(200, 1200)
        self.height = random.randint(200, 1200)
        self.test_image = create_test_image(self.width, self.height)

    def test_create_new_picture_without_width_height_in_request(self):
        picture_name = "my name"

        response = self.client.post(
            reverse("pictures-list"),
            data={"title": picture_name, "image": self.test_image},
        )
        self.assertEqual(response.status_code, 201)
        self.assertTrue(Picture.objects.filter(title=picture_name).exists())
        self.assertTrue(
            self.test_image.name in os.listdir(IMAGE_FOLDER_PATH),
            "Image not uploaded.",
        )

    def test_create_new_picture_with_width_height_in_request(self):
        picture_name = "my name"
        new_width = random.randint(200, 1200)
        new_height = random.randint(200, 1200)

        response = self.client.post(
            reverse("pictures-list"),
            data={
                "title": picture_name,
                "image": self.test_image,
                "width": new_width,
                "height": new_height,
            },
        )
        self.assertEqual(response.status_code, 201)
        self.assertTrue(Picture.objects.filter(title=picture_name).exists())
        picture_image = Picture.objects.filter(title=picture_name).first().image
        self.assertEqual(
            (picture_image.height, picture_image.width), (new_height, new_width)
        )

    def test_create_new_picture_with_invalid_width_height_in_request(self):
        picture_name = "my name"
        new_width = random.randint(-200, -100)
        new_height = random.randint(-300, -100)

        response = self.client.post(
            reverse("pictures-list"),
            data={
                "title": picture_name,
                "image": self.test_image,
                "width": new_width,
                "height": new_height,
            },
        )
        self.assertEqual(response.status_code, 400)
        self.assertFalse(Picture.objects.filter(title=picture_name).exists())

    def test_list_pictures(self):
        number_of_pictures = 10
        PictureFactory.create_batch(number_of_pictures)
        response = self.client.get(reverse("pictures-list"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get("count", -1), number_of_pictures)

    def test_retrieve_picture(self):
        picture = PictureFactory.create()
        expected_response = {
            "id": picture.id,
            "title": picture.title,
            "width": picture.image.width,
            "height": picture.image.height,
        }
        response = self.client.get(
            reverse("pictures-detail", kwargs={"pk": picture.id})
        )
        self.assertEqual(response.status_code, 200)
        self.assertDictContainsSubset(
            expected_response,
            response.json(),
        )
        self.assertTrue("url" in response.json())

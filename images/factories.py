import factory
from factory.django import DjangoModelFactory

from images.models import Picture


class PictureFactory(DjangoModelFactory):

    title = factory.Faker("company")
    image = factory.django.ImageField(width=1024, height=768)

    class Meta:
        model = Picture
